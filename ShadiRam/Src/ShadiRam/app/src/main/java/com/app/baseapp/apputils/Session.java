package com.app.baseapp.apputils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Session {

    public static final String FCMTOKEN = "";
    public static final String LANGUAGETEXT = "";
    private static final String TAG = "Name";
    static SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    static Context context;
    public final static String PREFS_NAME = "Artifie";
    public final static String LANGUAGE_TEXT = "language";
    public final static String CURRENT_USERID = "current_userid";
    public final static String USEREMAIL = "userEmail";
    public final static String MODULEVISIBLE = "modulevisible";
    public final static String SCHOOL_ID = "school_id";
    public final static String USERID = "userid";
    public final static String USERNAME = "user_name";
    public final static String USERIDNAME = "user_id_name";
    public final static String LANGUAGE = "";
    public final static String PASSWORD = "password";
    public final static String CLASS = "classs";
    public final static String CLASS_ID = "classs_id";
    public final static String SECTION = "section";
    public final static String FIRSTNAME = "first_name";
    public final static String LICENSEKEY = "license_key";
    public final static String SELECTPRESENT = "selectpresent";
    public final static String CURRENTDATE = "datesave";
    public final static String FIRSTTIME = "firsttime";
    public final static String LISTDATAKEY = "keyofList";
    public final static String INTERESTDATA = "interestdata";
    public final static String FASTTIMEINSTALLL = "firstTimeinstall";
    public final static String ANALYTICSCLASSID = "analyticsclassid";
    public final static String ANALYTICSCLASSNAME = "analyticsclassname";
    public final static String REFSECTION = "refsection";
    public final static String SUBJECTNAME = "subjectName";
    public final static String UDISECODE = "udiseCode";
    public final static String GENDER = "gender";
    public final static String TTPCODE = "ttpcode";
    public final static String SCHOOLNAME = "schoolname";
    public final static String SCHOOLBLOCK = "schoolBlock";
    public final static String SCHOOLDISTRICT = "schoolDistrict";
    public final static String SCHOOLSTATE = "schoolstate";
    public final static String CONTECTNUMBER = "contectNumber";
    public final static String ROLE = "role";
    public final static String filePath = "filepath";
    public final static String TEST_ASSIGNED = "test_assigned";
    public final static String COPIED_USERNAME = "";
    public final static String SUBJECT_NAME = "subject_name";
    public final static String VIDEO_DURATION = "VIDEO_DURATION";
    public final static String mTYPE = "mTYPE";
    public final static String mPERCENT = "mPERCENT";



    public Session(Context context){

        this.context = context;
        sharedPref = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        //  sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

    }



    public static void setInt(String key, int value) {

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }







    public  void setListInterestData(ArrayList<String> arrayList, String keyofList){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(arrayList);

        editor.putString(keyofList, json);
        editor.commit();
    }

    public ArrayList<String>  getListInterestData(String keyofList){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(keyofList, "");
        Type type = new TypeToken<List<String>>() {}.getType();
        ArrayList<String> arrayList = gson.fromJson(json, type);
        return arrayList;
    }


    public static void clearAll() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear().apply();
    }

    public void putBoolean(String key, boolean value) {
        sharedPref.edit().putBoolean(key, value).apply();
    }
    public synchronized boolean getBoolean(String key, boolean defaultValue) {
        return sharedPref.getBoolean(key, defaultValue);
    }
    public void putString(String key, String value) {
        sharedPref.edit().putString(key, value).apply();
    }
    public void clear() {
        sharedPref.edit().clear().apply();
    }

    public void remove(String key) {
        sharedPref.edit().remove(key).apply();
    }

    public String getString(String key) {
        return sharedPref.getString(key, "");
    }


    public void putLong(String key, long value) {
        sharedPref.edit().putLong(key, value).apply();
    }

    public long getLong(String key) {
        return sharedPref.getLong(key, 0);
    }

    public void setLoggedin(boolean logggedin) {
        editor.putBoolean("loggedInmode", logggedin);
        editor.commit();
    }

    public boolean loggedin() {
        return sharedPref.getBoolean("loggedInmode", false);
    }

    public void putInt(String key, int value) {
        sharedPref.edit().putInt(key, value).apply();
    }

    public int getInt(String key) {
        return sharedPref.getInt(key, 0);
    }

    public void setSelectedLanguage(String m){
        editor.putString("setLanguage",m);
        editor.commit();
    }
    public String getSelectedLanguage(){
        return sharedPref.getString("setLanguage","en");
    }

    public void setSelectedLanguageOnce(String m){
        editor.putString("setSelectedLanguageOnce",m);
        editor.commit();
    }

    public String getSelectedLanguageOnce(){
        return sharedPref.getString("setSelectedLanguageOnce","en");
    }

}
