package com.app.baseapp.feature.home_screen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.baseapp.R;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

import java.util.ArrayList;

import static com.app.baseapp.apputils.AppConstants.KEY_BUNDLE_DATA;


@SuppressLint("NonConstantResourceId")
@Layout(R.layout.swipe_cardview)
public class HomeSwipeCardView {

    @View(R.id.profileImageView_owner_trip)
    private ImageView profileImageView;

    @View(R.id.tripname_yourtrip)
    private TextView mTripName;

    @View(R.id.tripdate_yourtrip)
    private TextView mTripDate;

    @View(R.id.checklist_owner_trip)
            private Button mCheckListButton;

    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    private HomeDataModel mProfile;
    private int mPosition;
    ArrayList<String> mTripList;

    public HomeSwipeCardView(Context context, HomeDataModel profile, SwipePlaceHolderView swipeView, int mPosition, ArrayList<String> mTripList) {
        mContext = context;
        mProfile = profile;
        mSwipeView = swipeView;
        this.mPosition = mPosition;
        this.mTripList = mTripList;
    }

    public HomeSwipeCardView(Context mContext, HomeDataModel profile, SwipePlaceHolderView mSwipeView) {
        this.mContext = mContext;
        this.mProfile = profile;
        this.mSwipeView = mSwipeView;
    }


    @Resolve
    private void onResolved() {
//        Glide.with(mContext).load(mProfile.tripimage).into(profileImageView);
//        mTripName.setText(mProfile.tripName);
//        mTripDate.setText(mProfile.startTime + " to " + mProfile.endTime);

        profileImageView.setOnClickListener(view -> {
//            Intent intent = new Intent(OliverTravelApplication.getApp(), TripDetailActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putString("id",mProfile.id);
//            bundle.putString("memberId",mProfile.membersId);
//            intent.putExtra(KEY_BUNDLE_DATA, bundle);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            OliverTravelApplication.getApp().startActivity(intent);
        });
        mCheckListButton.setOnClickListener(view -> {
//            Intent intent = new Intent(OliverTravelApplication.getApp(), TripDetailsCheckPointActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putString("id",mProfile.id);
//            bundle.putString("memberId",mProfile.membersId);
//            intent.putExtra(KEY_BUNDLE_DATA, bundle);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            OliverTravelApplication.getApp().startActivity(intent);
        });
    }

    @SwipeOut
    private void onSwipedOut() {
        Log.d("EVENT", "onSwipedOut");
        mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState");
    }



    @SwipeIn
    private void onSwipeIn() {

        if (mTripList.size() - 1 == mPosition) {
            for (int i = 0; i < mTripList.size(); i++){
                HomeDataModel model = new HomeDataModel();
                mSwipeView.addView(new HomeSwipeCardView(mContext, model, mSwipeView,i,mTripList));
            }
        }

    }

    @SwipeInState
    private void onSwipeInState() {
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState() {
        Log.d("EVENT", "onSwipeOutState");
    }

}
