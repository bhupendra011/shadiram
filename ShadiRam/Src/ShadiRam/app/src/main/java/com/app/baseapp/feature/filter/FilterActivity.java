package com.app.baseapp.feature.filter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.app.baseapp.R;
import com.app.baseapp.baseui.BaseActivity;

public class FilterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        setUpToolBar("Filter",true);
        findViewById(R.id.filter).setVisibility(View.GONE);
    }
}