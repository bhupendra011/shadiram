package com.app.baseapp.feature;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import com.app.baseapp.R;
import com.app.baseapp.apputils.Session;
import com.app.baseapp.baseui.BaseActivity;
import com.app.baseapp.feature.landing_activity.HomeActivity;
import com.app.baseapp.feature.login_module.login_screen.LoginActivity;
import com.app.baseapp.preference.AppPreferences;

import java.util.Locale;


public class SplashActivity extends BaseActivity {

    private Handler handler;
    private Runnable runnable;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        session = new Session(getApplicationContext());
        SharedPreferences sp = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        setApplicationLocale(session.getSelectedLanguage());
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("first", true);
        editor.commit();
        handler = new Handler();
        runnable = () -> {
            if (AppPreferences.getPreferenceInstance(this).getIsUserLogin())
                switchActivity(HomeActivity.class);
            else
                switchActivity(SelectLanguageActivity.class);
            finish();
        };

    }

    /**
     * In this method we are applying delay of SPLASH_DISPLAY_LENGTH
     */
    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, SPLASH_DISPLAY_TIME);
    }


    /**
     * This method is called when activity is destroyed.
     * In this, callbacks of handler removed.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    private void setApplicationLocale(String locale) {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(new Locale(locale.toLowerCase()));
        } else {
            config.locale = new Locale(locale.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
    }
}
