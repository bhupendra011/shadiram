package com.app.baseapp.feature;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import com.app.baseapp.R;
import com.app.baseapp.apputils.LocaleHelper;
import com.app.baseapp.apputils.Session;
import com.app.baseapp.feature.login_module.login_screen.LoginActivity;

import java.util.Locale;

public class SelectLanguageActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgh, imge, back_buttonch;
    CardView btnchconfirm;
    String selectedLang = "en";
    String isFrom;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        session = new Session(getApplicationContext());
        init();
    }

    private void init() {
        imgh = (ImageView) findViewById(R.id.imgh);
        imge = (ImageView) findViewById(R.id.imge);
        back_buttonch = (ImageView) findViewById(R.id.back_buttonch);
        btnchconfirm = (CardView) findViewById(R.id.btnchconfirm);
        btnchconfirm.setOnClickListener(this);
        back_buttonch.setOnClickListener(this);
        imge.setOnClickListener(this);
        imgh.setOnClickListener(this);

        selectedLang = session.getSelectedLanguage();
        if (selectedLang.equalsIgnoreCase("en")){
            imge.setImageDrawable(getResources().getDrawable(R.drawable.img_english_orange_one));
            imgh.setImageDrawable(getResources().getDrawable(R.drawable.img_hindi_blue_one));
        }else{
            imge.setImageDrawable(getResources().getDrawable(R.drawable.img_english_blue_one));
            imgh.setImageDrawable(getResources().getDrawable(R.drawable.img_hindi_orange_one));
        }
        LocaleHelper.setLocale(SelectLanguageActivity.this,selectedLang);


        Intent i = getIntent();
        if (i != null) {
            isFrom = i.getStringExtra("fromMore");
//            if (isFrom.equalsIgnoreCase("Yes")) {
//                back_buttonch.setVisibility(View.VISIBLE);
//            } else {
//                back_buttonch.setVisibility(View.INVISIBLE);
//            }
        }
    }
    private void setApplicationLocale(String locale) {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(new Locale(locale.toLowerCase()));
        } else {
            config.locale = new Locale(locale.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
    }
    @Override
    public void onClick(View v) {
        if (v == back_buttonch) {
            finish();
        } else if (v == btnchconfirm) {
            LocaleHelper.setLocale(SelectLanguageActivity.this,selectedLang);
           // session.setSelectedLanguage(selectedLang);
            setApplicationLocale(selectedLang);
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
//            if (isFrom.equalsIgnoreCase("No")){
//                session.setSelectedLanguageOnce("yes");
//                SharedPreferences sp = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
//                if (!sp.getBoolean("first", false)) {
//                    SharedPreferences.Editor editor = sp.edit();
//                    editor.putBoolean("first", true);
//                    editor.commit();
//                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class); //call your ViewPager class
//                    startActivity(intent);
//                }else{
//                    if (session.loggedin()){
//                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                        startActivity(i);
//                        finish();
//                    }else{
//                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                        startActivity(i);
//
//                    }
//                }
//
//            }else {
//                Intent i = new Intent(SelectLanguageActivity.this, LoginActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//            }
        } else if (v == imge) {
            selectedLang = "en";
            imge.setImageDrawable(getResources().getDrawable(R.drawable.img_english_orange_one));
            imgh.setImageDrawable(getResources().getDrawable(R.drawable.img_hindi_blue_one));
        } else if (v == imgh) {
            selectedLang = "hi";
            imge.setImageDrawable(getResources().getDrawable(R.drawable.img_english_blue_one));
            imgh.setImageDrawable(getResources().getDrawable(R.drawable.img_hindi_orange_one));
        }
    }
}