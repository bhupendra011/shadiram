package com.app.baseapp.feature.home_screen;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baseapp.BaseAppApplication;
import com.app.baseapp.R;
import com.app.baseapp.apputils.BaseUtils;
import com.app.baseapp.baseui.BaseFragment;
import com.app.baseapp.baseui.BaseViewModelFactory;
import com.app.baseapp.feature.filter.FilterActivity;
import com.app.baseapp.feature.landing_activity.HomeActivity;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.Utils;

import java.util.ArrayList;

import javax.inject.Inject;

public class HomeFragment extends BaseFragment {

    private HomeAdapter mHomeAdapter;
    private RecyclerView mHomeRecyclerview;
    private ArrayList<String> mTripList;

    private Context mContext ;

    @Inject
    public BaseViewModelFactory mViewModelFactory;
    private SwipePlaceHolderView mSwipeView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.toolbar_up_btn_fragment).setOnClickListener(v -> openDrawer());
        setUpToolbar(getString(R.string.app_name));
        mTripList = new ArrayList<>();
        mTripList.add(String.valueOf(getResources().getDrawable(R.drawable.gettyimages)));
        mTripList.add(String.valueOf(getResources().getDrawable(R.drawable.hipster_72468415)));
        mTripList.add(String.valueOf(getResources().getDrawable(R.drawable.imafzwtyj5rhq4ek)));
        initView(view);

        //setUpRecyclerview(view);

    }

    private void initView(View view) {
        mSwipeView = view.findViewById(R.id.swipeView_owner);
        view.findViewById(R.id.filter).setOnClickListener(v -> {
            startActivity(FilterActivity.class);
        });

          mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(15)
                        .setRelativeScale(1.0f));

//        for(HomeDataModel profile : BaseUtils.loadProfilesOwner(this.mContext)){
//            mSwipeView.addView(new HomeSwipeCardView(mContext, profile, mSwipeView));
//        }

        setCardData(mTripList,mSwipeView);
    }

    private void setUpRecyclerview(View view) {
        mHomeRecyclerview = view.findViewById(R.id.recycler_view);
        mHomeAdapter = new HomeAdapter(getActivity());
        mHomeRecyclerview.setHasFixedSize(true);
        mHomeRecyclerview.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mHomeRecyclerview.setAdapter(mHomeAdapter);
    }

     public void setCardData(ArrayList<String> mTripList, SwipePlaceHolderView mSwipeView) {
        for (int i = 0; i < this.mTripList.size(); i++) {
            HomeDataModel model = new HomeDataModel();
//            mId = model.id;
//            mMember = model.membersId;
            this.mSwipeView.addView(new HomeSwipeCardView(mContext, model, this.mSwipeView, i, this.mTripList));
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        BaseAppApplication.getApp().getDaggerAppComponent().provideIn((HomeActivity) getActivity());

    }


}
